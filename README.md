# Geocoding and autocomplete with Google Maps and jQuery UI

A simple demo showing how to use the jQuery UI autocomplete widget in conjunction with Google Maps geocoding service.

[See it in action](http://therealkris.github.com/gmaps-autocomplete/)

Forked from this version: [rjshade.com/projects/gmaps-autocomplete/](http://rjshade.com/projects/gmaps-autocomplete/) and added a radius selection and resizable circle overlay showing the readius.

